import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import MainPage from './Pages/Main';
import { AppContextProvider } from './lib/AppContext';
import NavBar from './Components/NavBar';

import './App.css';
import 'react-toastify/dist/ReactToastify.css';

function App () {
	return (
		<AppContextProvider>
			<Router>
				<NavBar />
				<div className="App">
					<Switch>
						<Route exact path="/" component={MainPage} />
						<Route exact path="/page/:pageNumber" component={MainPage} />
					</Switch>
				</div>
			</Router>
			<ToastContainer />
		</AppContextProvider>
	);
}

export default App;
