import { useContext } from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import { useHistory } from 'react-router-dom';
import { AppContext } from '../lib/AppContext';

export default function TasksPagination () {
	const { pagesCount, page, setPage } = useContext(AppContext);
	const history = useHistory();

	const pageNumber = (i) => `/page/${i}`;

	const TasksPaginationLink = ({ targetPage, children, ...restProps }) => (
		<PaginationItem
			key={targetPage}
			active={page === targetPage}
		>
			<PaginationLink
				onClick={(e) => {
					e.preventDefault();
					history.push(pageNumber(targetPage));
				}}
				href={pageNumber(targetPage)}
				{...restProps}
			>
				{children}
			</PaginationLink>
		</PaginationItem>
	);

	return (
		<Pagination>
			{
				page !== 1 && (
					<>
						<TasksPaginationLink
							targetPage={1}
							first
						/>
						<TasksPaginationLink
							targetPage={page - 1}
							previous
						/>
					</>
				)
			}
			{
				new Array(pagesCount || 0).fill().map((e, i) => (
					<TasksPaginationLink
						targetPage={i + 1}
					>
						{i + 1}
					</TasksPaginationLink>
				))
			}
			{
				page !== pagesCount && (
					<>
						<TasksPaginationLink
							targetPage={page + 1}
							next
						/>
						<TasksPaginationLink
							targetPage={pagesCount}
							last
						/>
					</>
				)
			}
		</Pagination>
	);
}
