import { useState, useContext } from 'react';
import { Button, FormFeedback, FormGroup, Input } from 'reactstrap';
import { login } from '../lib/api';
import { AppContext } from '../lib/AppContext';
import { setCookie } from '../lib/cookie';

export default function LoginForm ({ onSuccess }) {
	const [form, setForm] = useState({});
	const [errors, setErrors] = useState(false);
	const { setIsAdmin } = useContext(AppContext);

	const setField = (field, value) => {
		delete errors[field];
		setErrors({ ...errors });
		form[field] = value;
		setForm({ ...form });
	};

	const handleSubmit = async (e) => {
		e.preventDefault();

		const data = await login(form.username, form.password);
		if (data.status === 'error') {
			setErrors({
				...data.message
			});
		} else {
			setCookie('isAdmin', data.message.token, 1);
			setIsAdmin(true);
			onSuccess();
		}
	};

	const validatedField = (label, field, restProps) => {
		const invalid = errors[field];
		return (
			<FormGroup>
				<label htmlFor={field}>{label}</label>
				<Input
					invalid={invalid}
					id={field}
					value={form[field]}
					onChange={(e) => setField(field, e.target.value)}
					{...restProps}
				/>
				{invalid && <FormFeedback>{invalid}</FormFeedback>}
			</FormGroup>
		);
	};

	return (
		<div>
			<h2>Авторизация</h2>
			<form onSubmit={handleSubmit}>
				{validatedField(
					'Имя пользователя',
					'username',
					{
						name: 'username'
					}
				)}
				{validatedField(
					'Пароль',
					'password',
					{
						type: 'password',
						name: 'password'
					}
				)}
				<br />
				<Button
					type="submit"
					color="primary"
					onClick={handleSubmit}
				>
					Войти
				</Button>
			</form>
		</div>
	);
}
