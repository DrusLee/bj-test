import { useState, useContext } from 'react';
import { Button, FormFeedback, FormGroup, Input } from 'reactstrap';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import { AppContext } from '../lib/AppContext';
import { createTask } from '../lib/api';

export default function CreateTaskForm ({ onSuccess }) {
	const [form, setForm] = useState({});
	const [errors, setErrors] = useState(false);
	const { tasks, setTasks, pagesCount, tasksCount, tasksCountPerPage } = useContext(AppContext);
	const history = useHistory();

	const setField = (field, value) => {
		delete errors[field];
		setErrors({ ...errors });
		form[field] = value;
		setForm({ ...form });
	};

	const handleSubmit = async (e) => {
		e.preventDefault();

		const data = await createTask(form.username, form.email, form.text);
		if (data.status === 'error') {
			setErrors({
				...data.message
			});
		} else {
			const _tasks = [...tasks, data.message];
			setTasks(_tasks);
			const newPagesCount = Math.ceil(((Number(tasksCount) + 1) || 0) / tasksCountPerPage);
			if (pagesCount < newPagesCount) {
				history.push(`/page/${newPagesCount}`);
			}

			toast.success('Новая задача успешно создана');
			onSuccess();
		}
	};

	const validatedField = (label, field, restProps) => {
		const invalid = errors[field];
		return (
			<FormGroup>
				<label htmlFor={field}>{label}</label>
				<Input
					invalid={invalid}
					id={field}
					value={form[field]}
					onChange={(e) => setField(field, e.target.value)}
					{...restProps}
				/>
				{invalid && <FormFeedback>{invalid}</FormFeedback>}
			</FormGroup>
		);
	};

	return (
		<div>
			<h2>Создать задачу</h2>
			<form onSubmit={handleSubmit}>
				{validatedField(
					'Имя пользователя',
					'username',
					{
						name: 'username'
					}
				)}
				{validatedField(
					'Email',
					'email',
					{
						type: 'email',
						name: 'email'
					}
				)}
				{validatedField(
					'Текст задачи',
					'text',
					{
						type: 'textarea',
						name: 'text'
					}
				)}
				<br />
				<Button
					type="submit"
					color="primary"
					onClick={handleSubmit}
				>
					Создать
				</Button>
			</form>
		</div>
	);
}
