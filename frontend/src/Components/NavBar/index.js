import { useContext } from 'react';
import { Modal, ModalBody, Nav, Navbar, NavbarBrand, NavItem, NavLink } from 'reactstrap';
import { AppContext } from '../../lib/AppContext';
import LoginForm from '../LoginForm';
import CreateTaskForm from '../CreateTaskForm';
import EditTaskForm from '../EditTaskForm';

export default function NavBar () {
	const {
		isAdmin,
		loginModalOpened,
		createTaskModalOpened,
		setLoginModalOpened,
		setCreateTaskModalOpened,
		logOut,
		setEditTaskId,
		editTaskId
	} = useContext(AppContext);

	return (
		<>
			<Navbar color="light" light expand="md" className="justify-content-between">
				<NavbarBrand href="/">Главная страница</NavbarBrand>
				<Nav>
					<NavItem>
						{
						isAdmin ? (
							<NavLink
								onClick={logOut}
							>
								Выйти
							</NavLink>
						) : (
							<NavLink
								onClick={() => setLoginModalOpened(true)}
							>
								Авторизоваться
							</NavLink>
						)
					}
					</NavItem>
				</Nav>
			</Navbar>
			{/* login modal */}
			<Modal
				isOpen={loginModalOpened}
				toggle={() => setLoginModalOpened(false)}
			>
				<ModalBody>
					<LoginForm
						onSuccess={() => setLoginModalOpened(false)}
					/>
				</ModalBody>
			</Modal>
			{/* create task modal */}
			<Modal
				isOpen={createTaskModalOpened}
				toggle={() => setCreateTaskModalOpened(false)}
			>
				<ModalBody>
					<CreateTaskForm
						onSuccess={() => setCreateTaskModalOpened(false)}
					/>
				</ModalBody>
			</Modal>
			{/* edit task modal */}
			<Modal
				isOpen={editTaskId}
				toggle={() => setEditTaskId(false)}
			>
				<ModalBody>
					<EditTaskForm
						onSuccess={() => setEditTaskId(false)}
					/>
				</ModalBody>
			</Modal>
		</>
	);
}
