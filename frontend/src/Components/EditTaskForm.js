import { useState, useContext, useEffect } from 'react';
import { Button, CustomInput, FormFeedback, FormGroup, Input } from 'reactstrap';
import { AppContext } from '../lib/AppContext';
import { createTask, editTask } from '../lib/api';

export default function EditTaskForm ({ onSuccess }) {
	const [form, setForm] = useState({});
	const [changed, setChanged] = useState({ });
	const [errors, setErrors] = useState(false);
	const { tasks, setTasks, editTaskId, checkIsAdmin } = useContext(AppContext);

	const completed = form.status === 10 || form.status === 11;

	useEffect(() => {
		if (!editTaskId) return;
		setForm((tasks || []).find((e) => e.id === editTaskId));
		setChanged({});
	}, [editTaskId]);

	const setField = (field, value) => {
		delete errors[field];
		setErrors({ ...errors });
		form[field] = value;
		changed[field] = value;
		setChanged({ ...changed });
		setForm({ ...form });
	};

	const handleSubmit = async (e) => {
		e.preventDefault();

		if (!checkIsAdmin(true)) return;

		const data = await editTask(editTaskId, changed);
		if (data.status === 'ok') {
			const _tasks = [...tasks];
			const ind = _tasks.findIndex((a) => a.id === editTaskId);
			Object.keys(changed).forEach((field) => {
				_tasks[ind][field] = changed[field];
			});
			setTasks(_tasks);
			onSuccess();
		}
	};

	const validatedField = (label, field, restProps) => {
		const invalid = errors[field];
		return (
			<FormGroup>
				<label htmlFor={field}>{label}</label>
				<Input
					invalid={invalid}
					id={field}
					value={form[field]}
					onChange={(e) => {
						setField('status', computeStatus(true, completed));
						setField(field, e.target.value);
					}}
					{...restProps}
				/>
				{invalid && <FormFeedback>{invalid}</FormFeedback>}
			</FormGroup>
		);
	};

	const computeStatus = (_edited, _completed) => (_completed ? 10 : 0) + (_edited ? 1 : 0);

	return (
		<div>
			<h2>Изменить задачу</h2>
			<form onSubmit={handleSubmit}>
				<CustomInput
					id={`statusCheckbox-${editTaskId}`}
					type="checkbox"
					label={completed ? 'Выполнено' : 'Не выполнено'}
					checked={completed}
					onChange={(e) => { // brain lag
						const { checked } = e.target;
						const edited = form.status === 1 || form.status === 11;
						const newStatus = computeStatus(edited, checked);
						setField('status', newStatus);
					}}
				/>
				{validatedField(
					'Текст задачи',
					'text',
					{
						name: 'text'
					}
				)}
				<br />
				<Button
					disabled={!Object.keys(changed).length}
					type="submit"
					color="primary"
					onClick={handleSubmit}
				>
					Сохранить
				</Button>
			</form>
		</div>
	);
}
