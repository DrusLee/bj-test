import { Badge, Button, Table } from 'reactstrap';
import { useContext, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { AppContext } from '../../lib/AppContext';
import TasksPagination from '../../Components/Pagination';

export default function MainPage () {
	const params = useParams();
	const {
		tasks,
		setPage,
		sortDirection,
		sortField,
		toggleSetSortField,
		callEditTask,
		callCreateTask,
		isAdmin
	} = useContext(AppContext);

	useEffect(() => {
		setPage(Number(params.pageNumber || 1));
	}, [params.pageNumber]);

	const ThSortable = ({ children, field, width = 20, ...props }) => (
		<th
			style={{ cursor: 'pointer', width: `${width}%` }}
			onClick={() => toggleSetSortField(field)}
			{...props}
		>
			{children} {field === sortField && (
				sortDirection === 'asc'
					? '↓'
					: '↑'
			)}
		</th>
	);

	const TaskStatus = ({ status }) => {
		const completed = status === 10 || status === 11;
		const edited = status === 1 || status === 11;

		return (
			<>
				<Badge color={completed ? 'success' : 'danger'} pill>{completed ? 'Выполнено' : 'Не выполнено'}</Badge>
				{edited && <Badge color="info" pill>Отредактировано администартором</Badge>}
			</>
		);
	};

    return (
	<div
		style={{
			display: 'flex',
			flexDirection: 'column',
			height: '400px'
		}}
	>
		<div
			style={{
				flex: 'auto'
			}}
		>
			<Table>
				<thead>
					<tr>
						<ThSortable field="username">
							Имя пользователя
						</ThSortable>
						<ThSortable field="email">
							email
						</ThSortable>
						<ThSortable
							field="text"
						>
							текст задачи
						</ThSortable>
						<ThSortable field="status">
							статус
						</ThSortable>
						<th>
							<Button onClick={callCreateTask}>
								+
							</Button>
						</th>
					</tr>
				</thead>
				<tbody>
					{
						tasks ? (
							tasks.map((task) => (
								<tr>
									<td>{task.username}</td>
									<td>{task.email}</td>
									<td>{task.text}</td>
									<td>
										<TaskStatus
											status={task.status}
										/>
									</td>
									<td>
										{
											isAdmin && (
												<Button onClick={() => callEditTask(task.id)}>
													*
												</Button>
											)
										}
									</td>
								</tr>
							))
						) : (
							<div>
								Загрузка...
							</div>
						)
					}
				</tbody>
			</Table>
		</div>
		<div>
			<TasksPagination />
		</div>
	</div>
    );
}
