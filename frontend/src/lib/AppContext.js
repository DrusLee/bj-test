import { createContext, useEffect, useState } from 'react';
import { getTasks, createTask, editTask } from './api';
import { getCookie, setCookie } from './cookie';

export const AppContext = createContext({});

const tasksCountPerPage = 3;

export function AppContextProvider ({ children }) {
	const [tasks, setTasks] = useState(null);
	const [tasksCount, setTasksCount] = useState(0);
	const [sortField, setSortField] = useState(null);
	const [sortDirection, setSortDirection] = useState('asc');
	const [pagesCount, setPagesCount] = useState(1);
	const [page, setPage] = useState(1);
	const [isAdmin, setIsAdmin] = useState(getCookie('isAdmin'));
	const [createTaskModalOpened, setCreateTaskModalOpened] = useState(false);
	const [loginModalOpened, setLoginModalOpened] = useState(false);
	const [editTaskId, setEditTaskId] = useState(false);

	const checkIsAdmin = (forceAuth) => {
		const _isAdmin = getCookie('isAdmin');
		if (isAdmin !== _isAdmin) {
			setIsAdmin(_isAdmin);
			if (forceAuth && !_isAdmin) setLoginModalOpened(true);
		}
		return _isAdmin;
	};

	const callCreateTask = () => {
		setCreateTaskModalOpened(true);
	};

	useEffect(() => { // reset sort direction when sortField changed
		setSortDirection('asc');
	}, [sortField]);

	const loadTasks = async () => {
		const data = await getTasks(sortField, sortDirection, page);
		if (data?.status) {
			setTasks([...data.message.tasks]);
			setTasksCount(data.message.total_task_count);
		}
	};

	useEffect(() => { // calculate tasksCount
		setPagesCount(Math.ceil((tasksCount || 0) / tasksCountPerPage));
	}, [tasksCount]);

	useEffect(() => { // reload tasks
		loadTasks();
	}, [sortField, sortDirection, page]);

	const callEditTask = (taskId) => {
		if (!checkIsAdmin()) {
			setLoginModalOpened(true);
		} else {
			setEditTaskId(taskId);
		}
	};

	const toggleSetSortField = (_sortField) => {
		if (sortField === _sortField) { // toggle sortDirection if chose same sortField
			setSortDirection(sortDirection === 'asc' ? 'desc' : 'asc');
		} else {
			setSortField(_sortField);
		}
	};

	const logOut = () => {
		setCookie('isAdmin', false, 0);
		setIsAdmin(false);
	};

    return (
	<AppContext.Provider
		value={{
			tasks,
			tasksCount,
			page,
			setPage,
			sortField,
			setSortField,
			sortDirection,
			setSortDirection,
			pagesCount,
			toggleSetSortField,
			callCreateTask,
			setCreateTaskModalOpened,
			setLoginModalOpened,
			createTaskModalOpened,
			loginModalOpened,
			isAdmin,
			setIsAdmin,
			logOut,
			tasksCountPerPage,
			setTasks,
			callEditTask,
			editTask,
			editTaskId,
			setEditTaskId,
			checkIsAdmin
		}}
	>
		{children}
	</AppContext.Provider>
    );
}
