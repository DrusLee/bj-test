import axios from 'axios';
import { getCookie } from './cookie';

const baseApiPath = 'https://uxcandy.com/~shapoval/test-task-backend/v2';

const developerName = 'Andrey';

const makeRequest = async ({ method = 'get', url = '', params = {} }) => {
	let constructedUrl = `${baseApiPath}${url}`;

	let _params = {};

	if (method === 'get') {
		_params.params = {
			...params,
			developer: developerName
		};
	} else {
		const formData = new FormData();

		Object.keys(params).forEach((key) => {
			formData.append(key, params[key]);
		});

		_params = {
			headers: { 'Content-Type': 'multipart/form-data' },
			data: formData
		};
		constructedUrl += `?developer=${developerName}`;
	}

	const { data } = await axios({
		method,
		url: constructedUrl,
		..._params
	});
	return data;
};

const getTasks = async (sort_field = 'id', sort_direction = 'asc', page = 1) => makeRequest({
	url: '/',
	params: {
		sort_field,
		sort_direction,
		page
	}
});

const createTask = async (username, email, text) => makeRequest({
	method: 'post',
	url: '/create',
	params: {
		username,
		email,
		text,
		token: getCookie('isAdmin')
	}
});

const editTask = async (id, { ...changed }) => makeRequest({
	method: 'post',
	url: `/edit/${id}`,
	params: {
		...changed,
		token: getCookie('isAdmin')
	}
});

const login = async (username, password) => makeRequest({
	method: 'post',
	url: '/login',
	params: {
		username, password
	}
});

export { makeRequest, getTasks, createTask, editTask, login };
